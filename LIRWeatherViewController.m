//
//  WeatherViewController.m
//
//
//  Created by manu on 15/05/2013.
//
//

#import "LIRWeatherViewController.h"
#import "LIRForecastClient.h"

@interface LIRWeatherViewController () <CLLocationManagerDelegate> {
    BOOL _locationFound;
}
@property (nonatomic, strong) NSDictionary *response;
@property (nonatomic, strong) NSArray *forecast;
@property (strong, nonatomic) NSDictionary *location;
@property (strong, nonatomic) CLLocationManager *locationManager;

@end

@implementation LIRWeatherViewController

#pragma mark - Controller methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialize location manager
        self.locationManager = [[CLLocationManager alloc] init];
        // Configuration of the location manager
        [self.locationManager setDelegate:self];
        [self.locationManager setDesiredAccuracy:kCLLocationAccuracyKilometer];
        // Add observer
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityStatusDidChange:) name:LIRReachabilityStatusDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(weatherDataDidChange:) name:LIRWeatherDataDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(temperatureUnitDidChange:) name:LIRTemperatureUnitDidChangeNotification object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.location = [[NSUserDefaults standardUserDefaults] objectForKey:LIRUserDefaultsLocation];
    if (!self.location)
        [self.locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    // Remove observer
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Observer/notifications methods

-(void)reachabilityStatusDidChange:(NSNotification *)notification {
    LIRForecastClient *forecastClient = [notification object];
    NSLog(@"Reachability status => %i", forecastClient.networkReachabilityStatus);
    // Update refresh button
    self.refreshButton.enabled = (forecastClient.networkReachabilityStatus!= AFNetworkReachabilityStatusNotReachable);
}

-(void)applicationDidBecomeActive:(NSNotification *)notification {
    if (self.location) {
        [self fetchWeatherData];
    }
}

-(void)weatherDataDidChange:(NSNotification *)notification {
    // Updating response
    [self setResponse:[notification userInfo]];
    // Updating forecast
    [self setForecast:self.response[@"hourly"][@"data"]];
    // Updating view
    [self updateView];
}

-(void)temperatureUnitDidChange:(NSNotification *)notification {
    // Update view
    [self updateView];
}


-(IBAction)refresh:(id)sender {
    if (self.location) {
        [self fetchWeatherData];
    }
}

-(void)setLocation:(NSDictionary *)location {
    if (!_location) _location = location;
    // Updating user defaults;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:location forKey:LIRUserDefaultsLocation];
    [ud synchronize];
    // Post notification
    NSNotification *notification1 = [NSNotification notificationWithName:LIRLocationDidChangeNotification object:self userInfo:location];
    [[NSNotificationCenter defaultCenter] postNotification:notification1];
    // Update view
    [self updateView];
    // Request location
    [self fetchWeatherData];
}

-(void)fetchWeatherData {
    if ([[LIRForecastClient sharedClient] networkReachabilityStatus] == AFNetworkReachabilityStatusNotReachable)
        return;
    // Showing progress hud
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    double latitude = [[_location objectForKey:LIRLocationKeyLatitude] doubleValue];
    double longitude = [[_location objectForKey:LIRLocationKeyLongitude] doubleValue];
    [[LIRForecastClient sharedClient] requestWeatherForCoordinate:CLLocationCoordinate2DMake(latitude, longitude) completion:^(BOOL success, NSDictionary *response) {
        // Dismiss HUD
        [SVProgressHUD dismiss];
        NSLog(@"Response > %@", response);
    }];
}

-(void)updateView {
    [self.labelLocation setText:[self.location objectForKey:LIRLocationKeyCity]];
}

#pragma mark -
#pragma mark LIRLocationsViewControllerDelegate methods

-(void)controller:(LIRLocationsViewController *)controller didSelectLocation:(NSDictionary *)location {
    self.location = location;
}

-(void)controllerShouldAddCurrentLocation:(LIRLocationsViewController *)controller {
    [self.locationManager startUpdatingLocation];
}

#pragma mark -
#pragma mark CLLocationManagerDelegate methods

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    if (![locations count] || _locationFound) return;
    // Stop updating location
    _locationFound = YES;
    [manager stopUpdatingLocation];
    // Current location
    CLLocation *currentLocation = [locations objectAtIndex:0];
    // Reverse geocoding
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        if ([placemarks count]) {
            _locationFound = NO;
            [self processPlacemark:[placemarks objectAtIndex:0]];
        }
    }];
}

-(void)processPlacemark:(CLPlacemark *)placemark {
    // Extracting the data
    NSString *city = [placemark locality];
    NSString *country = [placemark country];
    CLLocationDegrees lat = placemark.location.coordinate.latitude;
    CLLocationDegrees lon = placemark.location.coordinate.longitude;
    // Creating Location dictionary
    NSDictionary *currentLocationDictionary = @{LIRLocationKeyCity:city, LIRLocationKeyCountry:country, LIRLocationKeyLatitude:@(lat), LIRLocationKeyLongitude:@(lon)};
    // Add to locations
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableArray *locations = [NSMutableArray arrayWithArray:[ud objectForKey:LIRUserDefaultsLocations]];
    [locations addObject:currentLocationDictionary];
    [locations sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:LIRLocationKeyCity ascending:YES]]];
    [ud setObject:locations forKey:LIRUserDefaultsLocations];
    // Synchronization
    [ud synchronize];
    // Update current location
    self.location = currentLocationDictionary;
    // Post notifications
    NSNotification *notification2 = [NSNotification notificationWithName:LIRDidAddLocationNotification object:self userInfo:currentLocationDictionary];
    [[NSNotificationCenter defaultCenter] postNotification:notification2];
}

@end
