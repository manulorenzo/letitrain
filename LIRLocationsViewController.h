//
//  LocationsViewController.h
//  
//
//  Created by manu on 15/05/2013.
//
//

#import <UIKit/UIKit.h>

@protocol LIRLocationsViewControllerDelegate;

@interface LIRLocationsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) id<LIRLocationsViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@protocol LIRLocationsViewControllerDelegate <NSObject>

-(void)controllerShouldAddCurrentLocation:(LIRLocationsViewController *)controller;
-(void)controller:(LIRLocationsViewController *)controller didSelectLocation:(NSDictionary *)location;

@end
