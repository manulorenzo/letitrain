//
//  WeatherViewController.h
//  
//
//  Created by manu on 15/05/2013.
//
//

#import <UIKit/UIKit.h>
#import "LIRLocationsViewController.h"

@interface LIRWeatherViewController : UIViewController<LIRLocationsViewControllerDelegate>
@property (nonatomic, weak) IBOutlet UILabel *labelLocation;
@property (nonatomic, weak) IBOutlet UIButton *refreshButton;
@end
