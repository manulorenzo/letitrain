//
//  main.m
//  LetItRain
//
//  Created by manu on 15/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LIRAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LIRAppDelegate class]));
    }
}
