//
//  LIRConstants.h
//  LetItRain
//
//  Created by manu on 15/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LIRConstants : NSObject
#pragma mark -
#pragma mark User Defaults
extern NSString * const LIRUserDefaultsLocation;
extern NSString * const LIRUserDefaultsLocations;
#pragma mark -
#pragma mark Notifications
extern NSString * const LIRDidAddLocationNotification;
extern NSString * const LIRLocationDidChangeNotification;
#pragma mark -
#pragma mark Location Keys
extern NSString * const LIRLocationKeyCity;
extern NSString * const LIRLocationKeyCountry;
extern NSString * const LIRLocationKeyLatitude;
extern NSString * const LIRLocationKeyLongitude;
#pragma mark -
#pragma mark Forecast.io constants
extern NSString *const LIRForecastApiKey;
extern NSString *const LIRForecastUrl;
#pragma mark - AFNetworking
extern NSString *const LIRReachabilityStatusDidChangeNotification;
#pragma mark - Forecast View Controller constants
extern NSString *const LIRWeatherDataDidChangeNotification;
extern NSString *const LIRTemperatureUnitDidChangeNotification;
@end
