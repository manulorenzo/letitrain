//
//  LIRForecastClient.m
//  LetItRain
//
//  Created by manu on 26/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "LIRForecastClient.h"
#import "LIRConstants.h"

@implementation LIRForecastClient
+(LIRForecastClient *)sharedClient {
    static dispatch_once_t predicate;
    static LIRForecastClient *_sharedClient = nil;
    dispatch_once(&predicate, ^{
        _sharedClient = [self alloc];
        _sharedClient = [_sharedClient initWithBaseURL:[self baseURL]];
    });
    return _sharedClient;
}

+(NSURL *)baseURL {
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", LIRForecastUrl, LIRForecastApiKey]];
}

-(id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (self) {
        // Accept HTTP header
        [self setDefaultHeader:@"Accept" value:@"application/json"];
        // Register HTTP operation class
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
         // Reachability
        __weak typeof(self)weakSelf = self;
        [self setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            [[NSNotificationCenter defaultCenter] postNotificationName:LIRReachabilityStatusDidChangeNotification object:weakSelf];
        }];
    }
    return self;
}

-(void)requestWeatherForCoordinate:(CLLocationCoordinate2D)coordinate completion:(LIRForecastClientCompletionBlock)completion {
    NSString *path = [NSString stringWithFormat:@"%f,%f", coordinate.latitude, coordinate.longitude];
    [self getPath:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (completion) {
            completion(YES, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completion) {
            completion(NO, nil);
            NSLog(@"Unable to fetch weather data due to error %@ with user info %@", error, error.userInfo);
        }
    }];
}

@end