//
//  LIRConstants.m
//  LetItRain
//
//  Created by manu on 15/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "LIRConstants.h"

@implementation LIRConstants
#pragma mark -
#pragma mark User Defaults
NSString * const LIRUserDefaultsLocation = @"location";
NSString * const LIRUserDefaultsLocations = @"locations";
#pragma mark -
#pragma mark Notifications
NSString * const LIRDidAddLocationNotification = @"com.manu.LIRDidAddLocationNotification";
NSString * const LIRLocationDidChangeNotification = @"com.manu.LIRLocationDidChangeNotification";
#pragma mark -
#pragma mark Location Keys
NSString * const LIRLocationKeyCity = @"city";
NSString * const LIRLocationKeyCountry = @"country";
NSString * const LIRLocationKeyLatitude = @"latitude";
NSString * const LIRLocationKeyLongitude = @"longitude";
#pragma mark - 
#pragma mark Forecast.io constants
NSString *const LIRForecastApiKey = @"430b879dc6f3c8eedba9eed5bdf3f472";
NSString *const LIRForecastUrl = @"https://api.forecast.io/forecast";
#pragma mark - AFNetworking
NSString *const LIRReachabilityStatusDidChangeNotification = @"com.manu.LIRReachabilityStatusDidChangeNotification";
#pragma mark - Forecast View Controller constants
NSString *const LIRWeatherDataDidChangeNotification = @"com.manu.LIRWeatherDataDidChangeNotification";
NSString *const LIRTemperatureUnitDidChangeNotification = @"com.manu.LIRTemperatureUnitDidChangeNotification";
@end

