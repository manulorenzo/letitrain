//
//  LIRForecastClient.h
//  LetItRain
//
//  Created by manu on 26/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "AFHTTPClient.h"
typedef void (^LIRForecastClientCompletionBlock)(BOOL success, NSDictionary *response);
@interface LIRForecastClient : AFHTTPClient
#pragma mark -
#pragma mark Shared client
+(LIRForecastClient *)sharedClient;
#pragma mark -
#pragma mark Instance methods
-(void)requestWeatherForCoordinate:(CLLocationCoordinate2D)coordinate completion:(LIRForecastClientCompletionBlock)completion;
@end
