LetItRain
=========
**LetItRain** is an iOS app that allows the users to add a list of (editable) locations and track the current weather of each of them. 

It uses [CocoaPods](http://cocoapods.org/) in order to manage several libraries we are being used, specifically:

* [ViewDeck](https://github.com/Inferis/ViewDeck)
* [AFNetworking](https://github.com/AFNetworking/AFNetworking)
* [SVProgressHUD](https://github.com/samvermette/SVProgressHUD)