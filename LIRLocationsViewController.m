//
//  LocationsViewController.m
//
//
//  Created by manu on 15/05/2013.
//
//

#import "LIRLocationsViewController.h"

@interface LIRLocationsViewController ()
@property (strong, nonatomic) NSMutableArray *locations;
@end

@implementation LIRLocationsViewController

static NSString *LocationCell = @"LocationCell";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Load locations
        [self loadLocations];
        // Add observer
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddLocation:) name:LIRDidAddLocationNotification object:nil];
    }
    return self;
}

-(void)didAddLocation:(NSNotification *)notification {
    NSDictionary *location = [notification userInfo];
    [self.locations addObject:location];
    [self.locations sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:LIRLocationKeyCity ascending:YES]]];
    [self.tableView reloadData];
}

-(void)dealloc {
    // Remove observer
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (_delegate) _delegate = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupView];
}

-(void)setupView {
    // registering class for cell reuse
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:LocationCell];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadLocations {
    self.locations = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:LIRUserDefaultsLocations]];
}

-(BOOL)isCurrentLocation:(NSDictionary *)location {
    // Fetching current location
    NSDictionary *currentLocation = [[NSUserDefaults standardUserDefaults] objectForKey:LIRUserDefaultsLocation];
    if ([location[LIRLocationKeyLatitude] doubleValue] == [currentLocation[LIRLocationKeyLatitude] doubleValue]
        && [currentLocation[LIRLocationKeyLatitude] doubleValue] == [currentLocation[LIRLocationKeyLatitude] doubleValue]) {
        return YES;
    } else {
        return NO;
    }
}
#pragma mark -
#pragma mark UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ([self.locations count] + 1);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:LocationCell forIndexPath:indexPath];
    // Configure cell
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

-(void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)index {
    if (index.row == 0) {
        [cell.textLabel setText:@"Add current location"];
    } else {
        // Fetching location
        NSDictionary *location = [self.locations objectAtIndex:(index.row - 1)];
        // Configure cell
        [cell.textLabel setText:[NSString stringWithFormat:@"%@, %@", location[@"city"], location[@"country"]]];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        // Notify the delegate
        [self.delegate controllerShouldAddCurrentLocation:self];
    } else {
        // Fetch location
        NSDictionary *location = [self.locations objectAtIndex:(indexPath.row - 1)];
        // Notify delegate
        [self.delegate controller:self didSelectLocation:location];
    }
    // Show center view controller
    [self.viewDeckController closeLeftViewAnimated:YES];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > 0) {
        return YES;
    }
    // Fetch location
    NSDictionary *location = [[NSUserDefaults standardUserDefaults] objectForKey:LIRUserDefaultsLocation];
    return ![self isCurrentLocation:location];
}

-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

#pragma mark -
#pragma mark UITableViewDataSource - deleting rows methods

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
    if (row > 0) {
        return UITableViewCellEditingStyleDelete;
    }
    else {
        return UITableViewCellEditingStyleNone;
    }
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.locations removeObjectAtIndex:(indexPath.row - 1)];
        [[NSUserDefaults standardUserDefaults] setObject:self.locations forKey:LIRUserDefaultsLocations];
        //[self.tableView reloadData];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

-(void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView reloadData];
}

@end
