//
//  ForecastViewController.m
//  
//
//  Created by manu on 15/05/2013.
//
//

#import "LIRForecastViewController.h"

@interface LIRForecastViewController ()
@property (nonatomic, strong) NSDictionary *response;
@property (nonatomic, strong) NSArray *forecast;
@end

@implementation LIRForecastViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Add observer
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(weatherDataDidChangeChange:) name:LIRWeatherDataDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(temperatureUnitDidChange:) name:LIRTemperatureUnitDidChangeNotification object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)weatherDataDidChange:(NSNotification *)notification {
    // Updating response
    [self setResponse:[notification userInfo]];
    // Updating forecast
    [self setForecast:self.response[@"daily"][@"data"]];
    // Updating view
    [self updateView];
}

-(void)temperatureUnitDidChange:(NSNotification *)notification {
    // Update view
    [self updateView];
}

-(void)updateView {
    
}

@end
